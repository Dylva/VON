const Discord = require('discord.js');
module.exports = {
    guildAdd(c){
        c.on("guildMemberAdd", mem => {
            var channel = mem.guild.channels.find("name", "welcome");
            channel.send(`Welcome ${mem} to ${mem.guild}! We hope you enjoy your stay!`);
        });
    },
    guildRemove(c){
        c.on("guildMemberRemove", mem => {
            var channel = mem.guild.channels.find("name", "welcome");
            channel.send(`K bye :c`);
        });
    }
}