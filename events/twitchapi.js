const api = require('request');
const fs = require('fs');
const config = JSON.parse(fs.readFileSync("realconfig.json", "utf-8"));
module.exports = {check, check2}
const streamer = config.streamer;
const id = config.twitchid;

// Information for the request
function check(){
    var options = {
        url: `https://api.twitch.tv/helix/streams?user_login=${streamer}`,
        headers: {
            'Client-ID': id
        }
    };
    let interval = setInterval(() => {
        api(options, function(err, res, body){
            var r = JSON.parse(body);
            if(r.data.length === 1){
                console.log(r.data[0].title);
                console.log("Clearing interval")
                clearInterval(interval);
                check2();
            } else{
                console.log("offline");
            }
        });
      }, 5000);
      return interval;
}
function check2(){
    var options = {
        url: `https://api.twitch.tv/helix/streams?user_login=${streamer}`,
        headers: {
            'Client-ID': id
        }
    };
    let interval2 = setInterval(() => {
        api(options, function(err, res, body){
            var r = JSON.parse(body);
            if(r.data.length === 1){
                console.log("Still live");
                
            } else if(r.data.length === 0){
                console.log("Is offline");
                console.log("clear interval");
                clearInterval(interval2);
                check();
            }
        });
      }, 8000);
      return interval2;
}