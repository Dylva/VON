const Discord = require('discord.js');
const fs = require('fs');
module.exports = {
    commando(c, dir, p){
        c.commands = new Discord.Collection();

    fs.readdir(dir + "/commands", (err, files) => {
        if(err) console.log(err);

        // Checks the file extension in the folder "commands" to see if it's a js file
        let jsfiles = files.filter(f => f.split(".").pop() === "js");

        if(jsfiles.length <= 0) {
            console.log("No commands to load!");
            return;
        }
        console.log(`Loading ${jsfiles.length} commands!`);

        jsfiles.forEach((f, i) => {
            let props = require(dir + `/commands/${f}`);
            console.log(`${i + 1}: ${f} loaded!`); // Use to see that the commands are loading/loaded
            c.commands.set(props.help.name, props);
        });
        console.log(`${jsfiles.length} commands are loaded!`);
    });

    c.on("message", mes => {
        const m = mes.content;

        // Checks before excetuting commands
        if(mes.author.bot === true) return;
        if(m.startsWith(p) != true) return;
        if(mes.channel.type === "DM") return;
        
        let messageArray = m.split(/\s+/g);
        let command = messageArray[0];
        let args = messageArray.slice[1];

        let cmd = c.commands.get(command.slice(p.length).toLowerCase());

        if(cmd) {cmd.run(c, mes, args);}
        else { mes.reply("That is not a command!").then(msg => {
                msg.delete(10000);
            });
        }
        if(m.startsWith(p)){
            mes.delete(2000);
        }
    });
    }
}