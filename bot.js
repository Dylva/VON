const Discord = require('discord.js');
const fs = require('fs');
const c = new Discord.Client();
const dir = __dirname;
const guildMembers = require('./events/guildMembers');
const commands = require('./events/commands');
const twitch = require('./events/twitchapi');

// Change "realconfig.json" to "config.json"
const config = JSON.parse(fs.readFileSync("realconfig.json", "utf-8"));
const token = config.token;
const p = config.prefix;
const streamer = config.streamer;
const id = config.twitchid;

c.on("ready", () => {
    console.log(`${c.user} is ready!`);
});

commands.commando(c, dir, p);
guildMembers.guildAdd(c);
guildMembers.guildRemove(c);
twitch.check();


c.login(token);
