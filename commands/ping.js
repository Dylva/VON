const fs = require('fs');
const config = JSON.parse(fs.readFileSync("realconfig.json", "utf-8"));
module.exports.run = async (c, mes, args) => {
    mes.channel.send("You pinged me?").then(msg => {
        msg.delete(10000);
    });
}

module.exports.help = {
    name:"ping",
    usage:`${config.prefix}ping`
}